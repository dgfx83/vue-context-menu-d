## Vue Context Menu (dynamic)

***External libs** \
[v-click-outside]:(https://github.com/ndelvalle/v-click-outside) \
[@fortawesome]:(https://github.com/FortAwesome/vue-fontawesome)

### Install
```
npm install vue-context-menu-d
```

### Register global
```
import VueContextMenuD from 'vue-context-menu-d'
Vue.use(VueContextMenuD);
```

### Register in component
```
import VueContextMenuD from 'vue-context-menu-d'
...
export default {
  ...
  components: {
    VueContextMenuD
  }
  ...
}
```

### Add to template
```
<template>
  ...
  <span @click="showContextMenuD($event, ctx)"></span>
  <VueContextMenuD ref="contextMenu" @option-clicked="onContextMenuDOptionClicked" />
  ...
</template>
```

### Show context menu
```
...
export default {
  ...
  methods: {

    // Method to generate individual context menu options based on context, e.g. clicked on
    getOptionsForContext(ctx) {

      // Some default options
      const options = [
        { name: 'Add', action: 'add', icon: ['fas', 'plus'] },
        { name: 'Remove', action: 'remove', icon: ['far', 'trash-alt'] }
      ];

      // Add option based on context
      if (ctx.wet) {
        options.push({ name: 'Heat up', action: 'heat', icon: ['fas', 'fire'], options: [
          {
            name: '10%',
            value: 10
          },
          {
            name: '20%',
            value: 20
          },
          {
            name: '30%',
            value: 30
          }
        ]});
      }

      // Some other submenu
      options.push({ name: 'Mark', action: 'mark', options: [
          {
            name: 'green',
            value: 'green'
          },
          {
            name: 'blue',
            value: 'blue'
          },
          {
            name: 'red',
            value: 'red'
          }
        ]});

      // Devider
      options.push({ name: 'divider', type: 'divider' });

      // Some more options
      options.push({ name: 'Search', action: 'search', icon: ['fas', 'search'] });

      return options;
    },

    // Method to build and show individual context menu
    showContextMenuD(event, ctx) {
      const options = this.getOptionsForContext(ctx);
      this.$nextTick(() => this.$refs.contextMenu.open(event, ctx, options));
    }
  }
  ...
}
```

### Catch click event
```
...
export default {
  ...
  methods: {
    onContextMenuOptionClicked(event) {
      const { ctx, option, value } = event;
      alert(`You clicked ${option}:${value} for ${ctx}`);
    }
  }
  ...
}
```