import { library } from '@fortawesome/fontawesome-svg-core';

import {
  faFolder, faEnvelope, faTrashAlt
} from '@fortawesome/free-regular-svg-icons';

import {
  faChevronRight, faEnvelopeOpenText, faFire, faPlus, faSearch, faPencilAlt
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faFolder, faEnvelope, faTrashAlt, faChevronRight, faEnvelopeOpenText, faFire, faPlus, faSearch, faPencilAlt
);
