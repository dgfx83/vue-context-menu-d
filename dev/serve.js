import { createApp } from 'vue';
import Dev from './serve.vue';

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import "./fontawesome";


createApp(Dev)
  .component('fa', FontAwesomeIcon)
  .mount('#app');
